package com.devcamp.circlecylinderapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlecylinderapi.Circle;
import com.devcamp.circlecylinderapi.Cylinder;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CircleCylinderController {
    @GetMapping("/circle-area")
    public double getAreaApi(@RequestParam(required = true, name = "radius") double radius){
        Circle circle = new Circle(radius);
        return circle.getArea();
    }
    @GetMapping("/cylinder-volume")
    public double getVolumeApi(@RequestParam(required = true, name = "radius") double radius, @RequestParam(required = true, name = "height") double height){
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    } 
}
